$(document).ready(function() {
	$('.js-tabs-group').on('click', '.js-tab', function(e){
		$('.js-tab').toggleClass('m-active');
		$('.m-tab').toggleClass('m-active');
		e.preventDefault();
	});
	$('.panel-title').on('click', 'a', function(e){
		e.preventDefault();
	});
	$('.panel-title a').collapse({
	  toggle: false
	});
  
	
	
	var width = $(window).width();
	if (width < 680 ) {
		$(".js-products").carouFredSel({
			width: '100%',
			swipe: true,
			align: 'center',
			items: {
				min: 1,
				max: 2
			},
			auto: false,
			next: '#next-product',
			prev: '#prev-product',
	    	responsive: false
		});
	} else {
		$(".js-products").trigger('destroy');
	};
});
$(window).load(function(){
	$(".slider_1").carouFredSel({
		items: 1,
		auto: false,
	    pagination: {
	        container: $('.sliderNav_1'),
	        //anchorBuilder: false
	    },
		next: '#read_more_1',
    	responsive: true
	});
	$(".slider_2").carouFredSel({
		items: 1,
		auto: false,
	    pagination: {
	        container: $('.sliderNav_2'),
	        //anchorBuilder: false
	    },
		next: '#read_more_2',
    	responsive: true
	});
	$(".slider_3").carouFredSel({
		items: 1,
		auto: false,
	    pagination: {
	        container: $('.sliderNav_3'),
	        //anchorBuilder: false
	    },
		next: '#read_more_3',
    	responsive: true
	});
	$(window).resize(function(event) {
		var width = $(window).width();
		if (width < 680 ) {
			$(".js-products").carouFredSel({
				width: '100%',
				swipe: true,
				align: 'center',
				items: {
					min: 1,
					max: 2
				},
				next: '#next-product',
				prev: '#prev-product',
				auto: false,
		    	responsive: false
			});
		} else {
			$(".js-products").trigger('destroy');
		};
	});
});
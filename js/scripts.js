$(window).load(function(){
		$(".box-scroll-list").mCustomScrollbar({
			  horizontalScroll:true
		}); 
		
		$('.box-scroll-list').mCustomScrollbar("update");
	}); 

$(function(){
	//fancybox
	$('.tovar-fancybox, .news-view').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {}
		}
	});
		   
	if($.browser.msie){ 
		   $('input[placeholder]').each(function(){  
				
				var input = $(this);        
				
				$(input).val(input.attr('placeholder'));
						
				$(input).focus(function(){
					 if (input.val() == input.attr('placeholder')) {
						 input.val('');
					 }
				});
				
				$(input).blur(function(){
					if (input.val() == '' || input.val() == input.attr('placeholder')) {
						input.val(input.attr('placeholder'));
					}
				});
			});
			
		};
		if($.browser.msie){ 
		   $('textarea[placeholder]').each(function(){  
				
				var input = $(this);        
				
				$(input).val(input.attr('placeholder'));
						
				$(input).focus(function(){
					 if (input.val() == input.attr('placeholder')) {
						 input.val('');
					 }
				});
				
				$(input).blur(function(){
					if (input.val() == '' || input.val() == input.attr('placeholder')) {
						input.val(input.attr('placeholder'));
					}
				});
			});
			
		};
		
	$('input, textarea').each(function(){
			var placeholder = $(this).attr('placeholder');
			$(this).focus(function(){ $(this).attr('placeholder', ''); return false;});
			$(this).focusout(function(){			 
				$(this).attr('placeholder', placeholder);
				return false;
			});
		});	

	
		
	$('.photo-card-product__mini li a').click(function(){
            //srcBig=$(this).attr('data-big');
            //$(this).parents('.photo-card-product').find('.photo-card-product__big img').attr('src',srcBig);			
			$(this).parents('.photo-card-product').find('.photo-card-product__mini li.active').removeClass('active');
			$(this).parent().addClass('active');			
            //return false;
        });
	
	
	$('#foo1').carouFredSel({
			width: '100%',
			prev: '#prev1',
			next: '#next1',
			align: 'center',
			scroll: {
				items:2,
				pauseOnHover	: true		
			},
			items: {
			//	height: '30%',	//	optionally resize item-height
				visible: {
					min: 2,
					max: 7
				}
			}
		});
	
		
	
 
 		$('.fancybox').click(function(){ $('.box-scroll-list').mCustomScrollbar("update"); });
 
 
	
	
	$(".link-all-objects").toggle(function() {
	  $(this).parents('.photo-card-product').find('.hide').removeClass('hide').addClass('visibleItem');
	  $(this).text("Свернуть");
	  }, function() {
	  $(this).parents('.photo-card-product').find('.visibleItem').removeClass('visibleItem').addClass('hide');
	  $(this).text("Показать");
	 });
	
	$('.list-size-product li a').click(function(){
	  $('.tabCont').fadeOut(0);
	  $('.list-size-product li').removeClass('active'); 
	  var id = $(this).attr('href');
	  $(id).fadeIn();
	  $(this).parent().addClass('active'); 
	  return false;
	 });
	
	
	$('.list-size-product li a').click(function(){	
			$(this).parents('.list-size-product').find('li a.active').removeClass('active');
			$(this).addClass('active');			
            return false;
        });
	
	
	
	$('.view-more').click(function(){$(this).parents('.one-box-filter').find('.hide-item').slideToggle(); return false;});
	$('.filter-title').click(function(){$(this).parent().find('.one-box-filter__drop').slideToggle(), $(this).toggleClass('active'); return false;});
	$('.list-radio label').click(function(){	
			$(this).parents('.list-radio').find('label.active').removeClass('active');
			$(this).addClass('active');			
            return false;
        });
	$('.list-radio .jq-radio').click(function(){	
			$(this).parents('.list-radio').find('label.active').removeClass('active');
			$(this).parent().addClass('active');			
            return false;
        });
	$('.list-checkbox list-checkbox__name').click(function(){	
			$(this).parents('.list-checkbox__item').toggleClass('active');			
        });
	$('.list-checkbox .jq-checkbox').click(function(){	
			$(this).parents('.list-checkbox__item').toggleClass('active');			
        });
	
	$('.box-tabs-info-object li a').click(function(){
	  $('.tabCont2').fadeOut(0);
	  $('.box-tabs-info-object li a').removeClass('button-product_color'); 
	  var id = $(this).attr('href');
	  $(id).fadeIn();
	  $(this).addClass('button-product_color'); 
	  return false;
	 });
	
	
	$('.button-minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.button-plus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) + 1;
        count = count > ($input.attr("maxlength")) ? ($input.attr("maxlength")) : count;
        $input.val(count);
        $input.change();
        return false;
    });
	
	$('.long-button-more').click(function(){$(this).parent().find('.list-car__item-hide').removeClass('list-car__item-hide'); return false;});
	
	$('.box-tabs-standart li a').click(function(){
	  $('.tabCont2').fadeOut(0);
	  $('.box-tabs-standart li a').removeClass('button-product_color'); 
	  var id = $(this).attr('href');
	  $(id).fadeIn();
	  $(this).addClass('button-product_color'); 
	  return false;
	 });
	
	$('.long-button-more').click(function(){$(this).parent().find('.item-search-hide').removeClass('item-search-hide'); return false;});
	
	$('.box-tabs-news li a').click(function(){
	  $('.tabCont2').stop().fadeOut(0);
	  $('.box-tabs-news li a').removeClass('button-product_color'); 
	  var id = $(this).attr('href');
	  $(id).stop().fadeIn();
	  $(this).addClass('button-product_color'); 
	  return false;
	 });
	
	$('.line-buttons-tabs li a').click(function(){
	  $('.tabCont2').fadeOut(0);
	  $('.line-buttons-tabs li').removeClass('active'); 
	  var id = $(this).attr('href');
	  $(id).fadeIn();
	  $(this).parent().addClass('active'); 
	  return false;
	 });


	
});

var handler2 = function(){
		var ww = $(window).width();
		
		if (ww <= 767) {
			$('#news').carouFredSel({
				width: '100%',
				responsive:true,
				align: 'center',
				swipe: {
						onMouse: true,
						onTouch: true
					},
				scroll: {
					items:1,
					pauseOnHover	: true		
				},
				items: {
				//	height: '30%',	//	optionally resize item-height
					visible: {
						min: 1,
						max: 1
					}
				}
			});
	
		} 
		else{
			$("#news").trigger('destroy');
		}
		
		
		
	}
	$(window).bind('load', handler2);
	$(window).bind('resize', handler2);



//
	$(".wheels-info a.more").click(function(){
        var $el = $(this);
		$el.text($el.text() == "Свернуть" ? "Читать полностью": "Свернуть");		
        $(this).prev('.wheels-info-text').toggleClass('open');
        $(this).toggleClass('open');
        return false;
    });


$('.slider-size').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  centerMode: false,
  focusOnSelect: true,
    	   responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			 slidesToShow: 3
		  }
		},
		{
		  breakpoint: 1066,
		  settings: {
			 slidesToShow: 2
		  }
		},
		{
		  breakpoint: 920,
		  settings: {
			slidesToShow: 1
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 2
		  }
		},
		{
		  breakpoint: 558,
		  settings: {
			slidesToShow: 1
		  }
		},
		{
		  breakpoint: 340,
		  settings: {
			slidesToShow: 1
		  }
		}
	  ]
});